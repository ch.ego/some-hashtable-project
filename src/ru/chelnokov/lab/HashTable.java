package ru.chelnokov.lab;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HashTable<K, V> {
    private final int DEFAULT_INITIAL_CAPACITY = 16;
    private final float loadFactor = 0.7f;

    public transient List<Entry> table = new ArrayList<>();

    private int threshold = (int) (DEFAULT_INITIAL_CAPACITY * loadFactor);
    private int currentSize = 0;
    private int capacity = DEFAULT_INITIAL_CAPACITY;

    public HashTable(int initialCapacity) {
        this.resize(initialCapacity);
    }

    public HashTable() {
        this.resize(DEFAULT_INITIAL_CAPACITY);
    }

    private void resize(int newCapacity){
        this.capacity = newCapacity;
        this.threshold = (int) (this.capacity * loadFactor);

        if (this.table.size() != 0) {
            List<Entry> temp = new ArrayList<>(this.table);
            this.table = new ArrayList<>(Collections.nCopies(this.capacity, null));
            this.table.addAll(0, temp);
        } else {
            this.table = new ArrayList<>(Collections.nCopies(this.capacity, null));
        }
    }

    private int hash(K key){
        return (key.hashCode() & 0x7fffffff) % this.threshold;
    }

    public V get(K key){
        int hash = this.hash(key);

        while (this.table.get(hash) != null){
            if (this.table.get(hash).key.equals(key)){
                return this.table.get(hash).value;
            }

            hash++;
        }
        return null;
    }

    public void add(K key, V value){
        if (this.currentSize == this.threshold) {
            this.resize(this.capacity * 2);
        }

        Entry element = new Entry(key, value);
        int hash = this.hash(key);
        if (this.get(key) == null) {
            while (this.table.get(hash) != null) {
                hash++;
            }
        }

        this.table.set(hash, element);
        this.currentSize++;
    }

    private class Entry {
        K key;
        V value;

        public Entry(K key, V value){
            this.key = key;
            this.value = value;
        }
    }
}
