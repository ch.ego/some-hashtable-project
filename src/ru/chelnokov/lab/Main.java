package ru.chelnokov.lab;

public class Main {
    public static void main(String[] args) {
        HashTable<Integer, String> week = new HashTable<>();
        week.add(1,"lundi");
        week.add(2,"mardi");
        week.add(3,"mercredi");
        week.add(4,"jeudi");
        week.add(5,"vendredi");
        week.add(6,"samedi");
        week.add(7,"dimanche");

        System.out.println(week.get(3));
    }
}
